#ifndef SG_MUX_H
#define SG_MUX_H

#include "stm32f1xx_hal.h"

#define SG_MUX_MUXA_GPIOx			GPIOC
#define SG_MUX_MUXB_GPIOx			GPIOC
#define SG_MUX_MUXC_GPIOx			GPIOC
#define SG_MUX_MUXA_PINx			(1<<13)
#define SG_MUX_MUXB_PINx			(1<<14)
#define SG_MUX_MUXC_PINx			(1<<15)

//use this for MUX_A on PC13 which is used for RTC and should be configured specially
#define SG_MUX_PROBLEM_PC13_SOLVE

#define SG_MUX_GPIO_CLKEN()			__HAL_RCC_GPIOC_CLK_ENABLE()


void SG_MUX_init(void);
void SG_MUX_set(uint8_t channel_number);

#endif
