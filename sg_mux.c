#include "SG_MUX.h"

void SG_MUX_init(){
	SG_MUX_GPIO_CLKEN();

	GPIO_InitTypeDef GPIO_InitStruct;

	//MUX_A init
#ifdef SG_MUX_PROBLEM_PC13_SOLVE
	extern RTC_HandleTypeDef hrtc;
	hrtc.Init.OutPut = RTC_OUTPUTSOURCE_NONE;
	HAL_RTC_Init(&hrtc);
#endif
	GPIO_InitStruct.Pin = SG_MUX_MUXA_PINx;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SG_MUX_MUXA_GPIOx, &GPIO_InitStruct);


	//MUX_B init
	GPIO_InitStruct.Pin = SG_MUX_MUXB_PINx;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SG_MUX_MUXB_GPIOx, &GPIO_InitStruct);

	//MUX_C init
	GPIO_InitStruct.Pin = SG_MUX_MUXC_PINx;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SG_MUX_MUXC_GPIOx, &GPIO_InitStruct);
}
void SG_MUX_set(uint8_t channel_number){
	if (channel_number<8){
		if (channel_number & 0x1){
			SG_MUX_MUXA_GPIOx->ODR |= SG_MUX_MUXA_PINx;
		}else{
			SG_MUX_MUXA_GPIOx->ODR &= ~SG_MUX_MUXA_PINx;
		}
		if (channel_number & 0x2){
			SG_MUX_MUXB_GPIOx->ODR |= SG_MUX_MUXB_PINx;
		}else{
			SG_MUX_MUXB_GPIOx->ODR &= ~SG_MUX_MUXB_PINx;
		}
		if (channel_number & 0x4){
			SG_MUX_MUXC_GPIOx->ODR |= SG_MUX_MUXC_PINx;
		}else{
			SG_MUX_MUXC_GPIOx->ODR &= ~SG_MUX_MUXC_PINx;
		}
	}
}
